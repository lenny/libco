.PHONY: all install

DESTDIR ?=
PREFIX ?= $(DESTDIR)/usr
INCLUDEDIR ?= include
LIBDIR ?= lib
MAJ = 0
MIN = 0.1.0

all:
	$(CC) $(CFLAGS) libco.c -c -Wall -fpic -DLIBCO_MP
	$(CC) $(CFLAGS) $(LDFLAGS) libco.o -shared -Wl,-soname,libco.so.$(MAJ) -o libco.so.$(MIN)
	$(AR) rcs libco.a libco.o
	sed -e "s|@prefix@|$(PREFIX)|g" \
	    -e "s|@libdir@|$(PREFIX)/$(LIBDIR)|g" \
	    -e "s|@includedir@|$(PREFIX)/$(INCLUDEDIR)|g" \
	    libco.pc.in > libco.pc

install:
	install -d $(PREFIX)/$(INCLUDEDIR)/
	install -d $(PREFIX)/$(LIBDIR)/
	install -d $(PREFIX)/$(LIBDIR)/pkgconfig/
	install -m 0664 libco.so.$(MIN) $(PREFIX)/$(LIBDIR)/libco.so.$(MIN)
	install -m 0664 libco.pc $(PREFIX)/$(LIBDIR)/pkgconfig/
	ln -s libco.so.$(MIN) $(PREFIX)/$(LIBDIR)/libco.so.$(MAJ)
	ln -s libco.so.$(MIN) $(PREFIX)/$(LIBDIR)/libco.so
	install -m 0664 libco.a $(PREFIX)/$(LIBDIR)/
	install -m 0664 libco.h $(PREFIX)/$(INCLUDEDIR)/
